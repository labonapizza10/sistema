/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.xtec.ioc.sistema;

import java.io.*;
import java.net.*;

/**
 *
 * @author  Eduard Porlán <edu@ccbng.com>
 */
public class Sistema {

    public static void main(String[] args) {
        System.out.println("versió 0.1 del projecte sistema"); 	
        try {
            InetAddress adressa = InetAddress.getLocalHost();
            String hostname = adressa.getHostName();
            System.out.println("hostname="+hostname);
            System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
            System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
            System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
            System.out.println("Versió OS: " + System.getProperty("os.version"));
            System.out.println("Creació de la branca 00 del projecte sistema");
            System.out.println("Afegint més codi a la branca00 del projecte sistema");
        }
        catch (IOException e) {
		System.out.println("Exception occurred");
        }  
    }
}
